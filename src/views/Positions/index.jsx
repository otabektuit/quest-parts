import Header from "../../components/Header";
import Table from "./PositionsTable";
import { useNavigate } from "react-router-dom";
import CreateButton from "../../components/Buttons/CreateButton";
import CSelect from "../../components/CSelect";
import { useState } from "react";
import { Line } from "react-chartjs-2";
import { Box, Button, Grid, Typography } from "@mui/material";
import {
  _breakdownStyleTableData,
  _breakdownTableData,
  _chartData,
  _options,
  _trades
} from "../../utils/mock";
import RateCard from "../../components/RateCard";
import TradeTable from "../../components/TradeTable";
import GridTable from "../../components/GridTable";

const PositionsPage = () => {
  const navigate = useNavigate();
  const [stocks] = useState([
    {
      value: 0,
      label: "Equities"
    },
    {
      value: 1,
      label: "AQO"
    },
    {
      value: 2,
      label: "Matrix"
    }
  ]);
  const [selectedStock, setSelectedStock] = useState(0);
  const [filter, setFilter] = useState(1);

  const onChangeStock = (_, item) => {
    setSelectedStock(item.props.value);
  };

  return (
    <div className="PositionsPage">
      <Header
        title="Dashboard"
        extra={
          <CSelect
            value={selectedStock}
            options={stocks}
            onChange={onChangeStock}
            label="Stock"
            width={100}
          />
          // <CreateButton
          //   onClick={() => navigate(`/positions/create`)}
          //   title="Create position"
          // />
        }
      />

      <div style={{ padding: "20px" }}>
        <Box bgcolor="#fff" p={2} borderRadius="10px">
          <Grid container>
            <Grid item lg={12} mt={2}>
              <Box display="flex" gap="10px">
                <Button
                  variant={filter === 1 ? "contained" : "outlined"}
                  onClick={() => setFilter(1)}
                >
                  Today
                </Button>
                <Button
                  variant={filter === 30 ? "contained" : "outlined"}
                  onClick={() => setFilter(30)}
                >
                  Month to date
                </Button>
              </Box>
            </Grid>
          </Grid>

          <Grid container columnSpacing={2} mt={2}>
            {_trades.map((trade, t) => (
              <Grid item lg={3} key={t}>
                <RateCard {...trade} />
              </Grid>
            ))}
          </Grid>
          <Grid container mt={2}>
            <Grid item lg={12}>
              <Line options={_options} data={_chartData} />
            </Grid>
          </Grid>
          <Grid container mt={2}>
            <Grid item lg={12}>
              <Line options={_options} data={_chartData} />
            </Grid>
          </Grid>
          <Grid container mt={2}>
            <Grid item lg={12} width="100%">
              <Typography fontWeight="bold" fontSize="18px">
                Breakdown Sector
              </Typography>
            </Grid>
            <Grid item lg={12} width="100%" minHeight={300}>
              <GridTable />
            </Grid>
          </Grid>
          <Grid container mt={2}>
            <Grid item lg={12} width="100%">
              <Typography fontWeight="bold" fontSize="18px">
                Breakdown Style Sector
              </Typography>
            </Grid>
            <Grid item lg={12} width="100%" minHeight={300}>
              <GridTable />
            </Grid>
          </Grid>
        </Box>
      </div>
    </div>
  );
};

export default PositionsPage;
