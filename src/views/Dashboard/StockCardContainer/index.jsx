import { Grid } from "@mui/material";
import { _trades } from "../../../utils/mock";
import StockCard from "../../../components/StockCard";
import { useEffect, useState } from "react";

const StockCardContainer = () => {
  const [stocks, setStocks] = useState([]);

  useEffect(() => {
    const ws = new WebSocket("ws://localhost:9000");

    ws.onopen = function () {
      ws.send("we have been connected to the web-socket :)");
    };

    ws.onmessage = function (e) {
      const { data } = e;
      setStocks([...JSON.parse(data)]);
    };

    return () => {
      ws.close();
    };
  }, []);

  return (
    <Grid container columnSpacing={2} mt={2}>
      {stocks.map((trade, t) => (
        <Grid item lg={3} key={t}>
          <>
            {/* <RateCard {...trade} /> */}
            <StockCard {...trade} />
          </>
        </Grid>
      ))}
    </Grid>
  );
};

export default StockCardContainer;
