import { Box, Typography } from "@mui/material";
import classNames from "classnames";
import styles from "./style.module.scss";

const RateCard = ({ name, posPnl, tradingPnl, totalPnl, diff, varStat }) => {
  return (
    <Box className={styles.card}>
      <Box className={styles.header}>
        <Typography
          className={classNames(styles.name, { [styles.low]: diff < 0 })}
        >
          {name}
        </Typography>
        <Typography
          className={classNames(styles.diff, { [styles.low]: diff < 0 })}
        >
          {diff}%
        </Typography>
      </Box>
      <Box className={styles.body}>
        <Box display="flex" justifyContent="space-between">
          <Typography>POS PNL</Typography>
          <Typography fontWeight={700}>${posPnl}M</Typography>
        </Box>
        <Box display="flex" justifyContent="space-between" mt={1}>
          <Typography>TRADING PNL</Typography>
          <Typography fontWeight={700}>${tradingPnl}M</Typography>
        </Box>
        <Box display="flex" justifyContent="space-between" mt={1}>
          <Typography>TOTAL PNL</Typography>
          <Typography fontWeight={700}>${totalPnl}M</Typography>
        </Box>
        <Box display="flex" justifyContent="space-between" mt={1}>
          <Typography>VaR</Typography>
          <Typography fontWeight={700}>{varStat}%</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default RateCard;
