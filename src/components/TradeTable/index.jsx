import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Row from "../TableRow";

export default function TradeTable({ name, columns, data, hideColumns }) {
  return (
    <TableContainer component={Paper}>
      <Typography variant="h6" gutterBottom component="div">
        {name}
      </Typography>
      <Table aria-label="collapsible table">
        {!hideColumns && (
          <TableHead>
            <TableRow>
              <TableCell />
              {columns?.map((column, c) => (
                <TableCell key={c}>{column.name}</TableCell>
              ))}
            </TableRow>
          </TableHead>
        )}

        <TableBody>
          {data?.map((row) => (
            <Row key={row.name} row={row} columns={columns} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
