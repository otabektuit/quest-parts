import { Box, Typography } from "@mui/material";
import { useMemo } from "react";
import { Line } from "react-chartjs-2";
import { NumericFormat } from "react-number-format";

const options = {
  responsive: true,
  plugins: {
    legend: {
      display: false
    },
    title: {
      display: false
    },
    filler: {
      propagate: true
    },
    tooltip: {
      enabled: false
    }
  },
  elements: {
    point: {
      radius: 1
    },
    line: {
      borderJoinStyle: "miter",
      tension: 0.4
    }
  },
  scales: {
    x: {
      border: { display: false },
      ticks: { display: false },
      grid: {
        display: false
      }
    },

    y: {
      border: { display: false },
      ticks: { display: false },
      grid: {
        display: false
      }
    }
  },
  filler: {
    propagate: false
  }
};

const labels = ["January", "February", "March", "April", "May", "June", "July"];

const StockCard = ({
  title = "POSTS",
  percent = 3.5,
  price = 2560,
  data = [-10, -5, 0, -1, 3, 5, 2],
  bgColor = "rgba(53, 162, 235, 0.5)",
  borderColor = "rgb(53, 162, 235)" //rgb(255, 99, 132)
}) => {
  const staticData = useMemo(
    () => ({
      labels,
      datasets: [
        {
          fill: true,
          label: null,
          data,
          borderColor:
            percent <= 0 ? "rgba(255, 99, 132,0.8)" : "rgba(26, 193, 157,0.8)",
          backgroundColor:
            percent <= 0 ? "rgba(255, 99, 132,0.2)" : "rgba(26, 193, 157,0.2)"
        }
      ]
    }),
    [data, bgColor, borderColor]
  );

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      border="1px solid rgba(0,0,0,0)"
      position="relative"
      minHeight={150}
      overflow="hidden"
      borderRadius={2}
      boxShadow="0 0 2px 2px rgba(0,0,0,0.06)"
    >
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        zIndex={2}
      >
        <Typography fontSize="14px" mb={1} color="grey" fontWeight={600}>
          {title}
        </Typography>
        <Typography fontSize="24px" mb={1} fontWeight={800}>
          <NumericFormat
            value={price}
            displayType={"text"}
            thousandSeparator={true}
            decimalScale={4}
            prefix="$"
          />
        </Typography>
        <Typography
          fontSize="14px"
          mb={1}
          fontWeight={600}
          color={percent < 0 ? "error" : "#1AC19D"}
        >
          <NumericFormat
            value={percent}
            displayType={"text"}
            thousandSeparator={true}
            decimalScale={4}
            suffix="%"
          />
        </Typography>
      </Box>
      <Box
        position="absolute"
        bottom={-30}
        left={0}
        zIndex={0}
        style={{
          transform: "scaleY(0.6) scaleX(1)"
        }}
      >
        <Line options={options} data={staticData} />
      </Box>
    </Box>
  );
};

export default StockCard;
