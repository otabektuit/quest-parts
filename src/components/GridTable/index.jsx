import React, { useCallback, useMemo, useRef, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";

const GridTable = () => {
  const containerStyle = useMemo(() => ({ width: "100%", height: "100%" }), []);
  const gridStyle = useMemo(() => ({ height: "100%", width: "100%" }), []);
  const [rowData, setRowData] = useState();

  const data = [
    {
      system: "NEO",
      sector: "Technology",
      category: "Top Winners",
      category_return: 3.28,
      symbol: "AAPL US",
      pnl: 4.8,
      attributions: 6.2,
      gross: 8.8
    }
  ];

  const [columnDefs, setColumnDefs] = useState([
    { field: "system", rowGroup: true, hide: true },
    { field: "sector", rowGroup: true, hide: true },
    { field: "category" },
    { field: "category_return", headerName: "Category Return" },
    { field: "symbol" },
    { field: "pnl" },
    { field: "attributions" },
    { field: "gross" }
  ]);

  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 100,
      sortable: true,
      resizable: true
    };
  }, []);
  const autoGroupColumnDef = useMemo(() => {
    return {
      minWidth: 200
    };
  }, []);

  const onGridReady = useCallback((params) => {
    // fetch("https://www.ag-grid.com/example-assets/olympic-winners.json")
    //   .then((resp) => resp.json())
    //   .then((data) => setRowData(data));
    setRowData(data);
  }, []);

  return (
    <div style={containerStyle}>
      <div style={gridStyle} className="ag-theme-alpine">
        <AgGridReact
          rowData={rowData}
          columnDefs={columnDefs}
          defaultColDef={defaultColDef}
          autoGroupColumnDef={autoGroupColumnDef}
          animateRows={true}
          onGridReady={onGridReady}
        ></AgGridReact>
      </div>
    </div>
  );
};

export default GridTable;
