import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import AlertProvider from "./providers/AlertProvider";
import GlobalFunctionsProvider from "./providers/GlobalFunctionsProvider";
import MaterialUIProvider from "./providers/MaterialUIProvider";
import Router from "./router";
import { persistor, store } from "./store";
import "./i18next";

import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-alpine.css"; // Optional theme CSS

import { Suspense } from "react";
import PageFallback from "./components/PageFallback";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler
} from "chart.js";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler
);

function App() {
  return (
    <Suspense fallback={<PageFallback />}>
      <div className="App">
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <MaterialUIProvider>
              <AlertProvider>
                <GlobalFunctionsProvider />
                <BrowserRouter>
                  <Router />
                </BrowserRouter>
              </AlertProvider>
            </MaterialUIProvider>
          </PersistGate>
        </Provider>
      </div>
    </Suspense>
  );
}

export default App;
