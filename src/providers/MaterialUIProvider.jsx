import { LocalizationProvider } from "@mui/x-date-pickers";
import ThemeConfig from "../theme/index";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

const MaterialUIProvider = ({ children }) => {
  const theme = "light";

  return (
    <div className={theme === "dark" ? "night-mode" : ""}>
      <ThemeConfig>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          {children}
        </LocalizationProvider>
      </ThemeConfig>
    </div>
  );
};

export default MaterialUIProvider;
