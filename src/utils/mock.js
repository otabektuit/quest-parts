export const _trades = [
  {
    name: "NEO",
    title: "NEO",
    price: 2560,
    percent: 4,
    posPnl: 7.41,
    tradingPnl: 2.31,
    totalPnl: 8.11,
    diff: 7,
    varStat: 8
  },
  {
    name: "NEO .2",
    title: "NEO .2",
    price: 182,
    percent: -4,
    posPnl: 3.41,
    tradingPnl: 5.31,
    totalPnl: 8.11,
    diff: -12,
    varStat: 9
  },
  {
    name: "VQM",
    title: "VQM",
    price: 8147,
    percent: 12.4,
    posPnl: 1.21,
    tradingPnl: 2.31,
    totalPnl: 3.11,
    diff: -5,
    varStat: 4
  }
];

const labels = ["January", "February", "March", "April", "May", "June", "July"];

export const _options = {
  responsive: true,
  plugins: {
    legend: {
      position: "bottom"
    },
    title: {
      display: true,
      text: "NEO PnL - Sector"
    }
  }
};

export const _chartData = {
  labels,
  datasets: [
    {
      label: "EMU USATR_CROWD",
      data: [0.5, 0.3, 1, 4, 5, 0, 1],
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 99, 132, 0.5)"
    },
    {
      label: "EMU USATR_MIDCAP",
      data: [0.5, -0.3, -1, -4, 5, 0, 1],
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)"
    }
  ]
};

export const breakdownColumns = [
  { name: "SYSTEM", key: "system" },
  { name: "SECTOR", key: "sector" },
  { name: "CATEGORY", key: "category" },
  { name: "CATEGORY RETURN", key: "categoryReturn" },
  { name: "SYMBOL", key: "symbol" },
  { name: "$PnL(K)", key: "pnl" },
  { name: "ATTRIBUTIONS(%)", key: "attributions" },
  { name: "GROSS BOOK SIZE(M)", key: "grossSize" }
];

const fourthLevel = {
  columns: breakdownColumns,
  data: [
    {
      categoryReturn: 3.25,
      symbol: "AAPL US",
      pnl: 4.8,
      attributions: 6.2,
      grossSize: 8.8,
      children: null
    }
  ],
  name: null
};

const thirdLevel = {
  columns: breakdownColumns,
  data: [
    {
      category: "Top Winners(1)",
      categoryReturn: 6.48,
      symbol: "",
      pnl: 4.8,
      attributions: 6.2,
      grossSize: 8.8,
      children: fourthLevel
    }
  ],
  name: null
};

const secondLevel = {
  columns: breakdownColumns,
  data: [
    {
      sector: "Technology(1)",
      category: null,
      categoryReturn: 12.96,
      symbol: "",
      pnl: 9.6,
      attributions: 12.4,
      grossSize: 17.6,
      children: thirdLevel
    }
  ],
  name: null
};

export const _breakdownTableData = {
  columns: breakdownColumns,
  data: [
    {
      system: "NEO(1)",
      sector: "",
      category: "",
      categoryReturn: 38.88,
      symbol: "",
      pnl: 28.8,
      attributions: 37.2,
      grossSize: 53.28,
      children: secondLevel
    }
  ],
  name: "Breakdown - Sector"
};

export const breakdownStyleColumns = [
  { name: "SYSTEM", key: "system" },
  { name: "STYLE FACTOR", key: "styleFactor" },

  { name: "$PnL(K)", key: "pnl" },
  { name: "ATTRIBUTIONS(%)", key: "attributions" },
  { name: "GROSS EXPOSURE(M)", key: "grossExposure" },
  { name: "NET EXPOSURE(M)", key: "netExposure" },
  { name: "FACTOR RETURN(%)", key: "factorReturn" }
];

const styleThirdLevel = {
  columns: breakdownStyleColumns,
  data: [
    {
      system: null,
      styleFactor: null,
      pnl: 28.8,
      attributions: 37.2,
      grossExposure: 37.2,
      netExposure: 37.2,
      factorExposure: 53.28,
      children: null
    }
  ],
  name: null
};

const styleSecondLevel = {
  columns: breakdownStyleColumns,
  data: [
    {
      system: null,
      styleFactor: "Beta",
      pnl: 28.8,
      attributions: 37.2,
      grossExposure: 37.2,
      netExposure: 37.2,
      factorExposure: 53.28,
      children: styleThirdLevel
    },
    {
      system: null,
      styleFactor: "Crowd",
      pnl: 28.8,
      attributions: 37.2,
      grossExposure: 37.2,
      netExposure: 37.2,
      factorExposure: 53.28,
      children: styleThirdLevel
    }
  ],
  name: null
};

export const _breakdownStyleTableData = {
  columns: breakdownStyleColumns,
  data: [
    {
      system: "NEO(2)",
      styleFactor: "",
      pnl: 28.8,
      attributions: 37.2,
      grossExposure: 37.2,
      netExposure: 37.2,
      factorExposure: 53.28,
      children: styleSecondLevel
    }
  ],
  name: "Breakdown - Style Factors"
};
